jQuery.noConflict();

(function(window, $) {

    /* Services
    -------------------------------------------------------------- */

    var Service = function(base_url, id_ldap, token) {
        if (base_url[base_url.length - 1] == '/') {
            base_url = base_url.substring(0, base_url.length - 1);
        }
        this.base_url = base_url;
        this.id_ldap = id_ldap;
        this.token = token;
    };

    Service.prototype.getCalendars = function(callback) {
        var id_ldap = this.id_ldap;
        var token = this.token;

        $.ajax({
            type: "GET",
            url: this.base_url + '/users/calendars',
            dataType: 'json',
            beforeSend: function(request) {
                request.setRequestHeader("idldap", id_ldap);
                request.setRequestHeader("token", token);
            },
            success: function(data) {
                callback(null, data.calendars);
            },
            error: function(err) {
                callback(err);
            }
        });
    };

    Service.prototype.setCalendars = function(calendars, callback) {
        var id_ldap = this.id_ldap;
        var token = this.token;
        var data = {
            calendars: calendars
        };

        $.ajax({
            type: "PUT",
            url: this.base_url + '/users/calendars',
            data: JSON.stringify(data),
            beforeSend: function(request) {
                request.setRequestHeader("idldap", id_ldap);
                request.setRequestHeader("token", token);
            },
            success: function(data) {
                callback(null);
            },
            error: function(err) {
                callback(err);
            }
        });
    };

    Service.prototype.getCalendar = function(id, centreId, callback) {
        var id_ldap = this.id_ldap;
        var token = this.token;

        var query = '';
        if (centreId) {
            query = '?centreId=' + centreId;
        }

        $.ajax({
            type: "GET",
            url: this.base_url + '/users/calendars/' + id + query,
            dataType: 'json',
            beforeSend: function(request) {
                request.setRequestHeader("idldap", id_ldap);
                request.setRequestHeader("token", token);
            },
            success: function(data) {
                callback(null, data.events);
            },
            error: function(err) {
                callback(err);
            }
        });
    };

    /* Agenda
    -------------------------------------------------------------- */

    /** jQuery object */
    var _element;
    /** liste des calendriers sélectionnées */
    var _selectdedCalendars = [];
    /** liste des événements classés par calendrier */
    var _events = {};
    /** calendar élément DOM */
    var _calendar;

    var _service;

    var _modal = new tingle.modal();

    /** liste des calendriers */
    var _calendars = [];

    var _centreId = null;

    /**
     * Initialise un agenda
     * @param  NddeDOM element
     */
    function init(element, centreId) {
        _element = $(element);
        _centreId = centreId;

        _service = new Service(element.getAttribute('data-url'), element.getAttribute('data-user'), element.getAttribute('data-token'));

        // init html
        var html = nunjucks.render('template.html');
        _element.html(html);

        // binds events
        _element.on('click', 'input', toggleCalendar);

        // get data
        _service.getCalendars(function(err, calendars) {
            if (err) {
                console.error(err);
                _calendars = [];
                updateView(_calendars);
                initCalendar();

                // stop loader
                document.querySelector('.js-calendar-loader').classList.remove('w-calendar--loading');

            } else {
                for (var i = 0; i < calendars.length; i++) {
                    if (calendars[i].selected) {
                        _selectdedCalendars.push(calendars[i].id);
                    }
                }
                _calendars = calendars;
                updateView(calendars);

                initCalendar();
                loadEvents();

                // stop loader
                document.querySelector('.js-calendar-loader').classList.remove('w-calendar--loading');
            }
        });
    };

    function setCentreId(centreId) {
        _centreId = centreId;
        for (var i = 0; i < _calendars.length; i++) {
            if (_calendars[i].key == 'staff.centre' && _calendars[i].selected) {
                var id = _calendars[i].id;
                _service.getCalendar(id, _centreId, function(err, events) {
                    if (err) {
                        console.error(err);
                    } else {
                        var color = getColor(Object.keys(_events).length);
                        events.map(function(event) {
                            event.color = color;
                            return event;
                        });

                        _events[id] = events;
                        _calendar.fullCalendar('addEventSource', _events[id]);
                    }
                });
            }
        }
        updateView(_calendars);
    }

    /**
     * Initialisation du calendrier
     */
    function initCalendar() {
        _calendar = _element.find('.js-calendar');
        _calendar.fullCalendar({
            lang: 'fr',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            displayEventTime: false,
            eventClick: function(calEvent, jsEvent, view) {
                calEvent.startDate = moment(calEvent.start);
                calEvent.endDate = moment(calEvent.end);
                if (calEvent.description) {
                    calEvent.description = calEvent.description.replace(/\\\"/g, '"');
                }

                var html = nunjucks.render('modal.html', {
                    event: calEvent
                });

                _modal.init();
                _modal.setContent(html);
                _modal.open();
            }
        });
    }

    /**
     * Chargement des événements
     */
    function loadEvents() {
        for (var i = 0; i < _selectdedCalendars.length; i++) {
            (function(id) {
                _service.getCalendar(id, _centreId, function(err, events) {

                    if (err) {
                        console.error(err);
                    } else {
                        var color = getColor(Object.keys(_events).length);
                        events.map(function(event) {
                            event.color = color;
                            return event;
                        });

                        _events[id] = events;
                        _calendar.fullCalendar('addEventSource', _events[id]);
                    }
                });
            })(_selectdedCalendars[i]);
        }
    }

    /**
     * Update de la vue
     * @param  Array calendars
     */
    function updateView(calendars) {
        if (_centreId == null) {
            calendars = calendars.filter(function(calendar) {
                return calendar.key != 'staff.centre';
            });
        }
        var html = nunjucks.render('calendars.html', {
            calendars: calendars
        });
        _element.find('.js-calendar-list').html(html);
    }

    /**
     * Action sélectionner/désélectionner un calendrier
     */
    function toggleCalendar() {
        var index = _selectdedCalendars.indexOf(this.value);
        var id = this.value;
        if (this.checked && -1 == index) {
            _selectdedCalendars.push(this.value);

            if (_events.hasOwnProperty(id)) {
                _calendar.fullCalendar('addEventSource', _events[id]);
            } else {
                _service.getCalendar(id, _centreId, function(err, events) {
                    if (err) {
                        console.error(err);
                    } else {
                        var color = getColor(Object.keys(_events).length);
                        events.map(function(event) {
                            event.color = color;
                            return event;
                        });

                        _events[id] = events;
                        _calendar.fullCalendar('addEventSource', _events[id]);
                    }
                });
            }

        } else if (!this.checked && -1 != index) {
            _selectdedCalendars.splice(index, 1);

            if (_events.hasOwnProperty(id)) {
                _calendar.fullCalendar('removeEventSource', _events[id]);
            } else {
                _service.getCalendar(id, _centreId, function(err, events) {
                    if (err) {
                        console.error(err);
                    } else {
                        var color = getColor(Object.keys(_events).length);
                        events.map(function(event) {
                            event.color = color;
                            return event;
                        });

                        _events[id] = events;
                        _calendar.fullCalendar('removeEventSource', _events[id]);
                    }
                });
            }
        }

        _service.setCalendars(_selectdedCalendars, function(err) {
            if (err) {
                console.error(err);
            }
        });
    }

    function getColor(n) {
        var colors = ['#3498db', '#95a5a6', '#9b59b6', '#e74c3c', '#f39c12'];
        var i = n % colors.length;
        return colors[i];
    }


    window.Agenda = {
        init: init,
        setCentreId: setCentreId,
    };

})(window, jQuery);
