var gulp = require('gulp');
var nunjucks = require('gulp-nunjucks');
var concat = require('gulp-concat');
var order = require('gulp-order');
var eventStream = require('event-stream');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');

gulp.task('default', ['js', 'css']);

gulp.task('watch', function() {
    gulp.watch(['src/**/*.js', 'src/**/*.html'], ['js']);
    gulp.watch('src/**/*.css', ['css']);

});

gulp.task('js', function() {
    var vendor = [
        'bower_components/jquery/dist/jquery.js',
        'bower_components/moment/moment.js',
        'bower_components/fullcalendar/dist/fullcalendar.js',
        'bower_components/fullcalendar/dist/lang/fr.js',
        'bower_components/nunjucks/browser/nunjucks.js',
        'bower_components/tingle/dist/tingle.js'
    ];

    var vendorFiles = gulp.src(vendor)
    .pipe(concat('vendor.js'));

    var templateFiles = gulp.src('src/*.html')
    .pipe(nunjucks())
    .pipe(concat('templates.js'));

    var appFiles = gulp.src('src/*.js')
    .pipe(concat('app.js'));

    return eventStream.concat(vendorFiles, templateFiles, appFiles)
    .pipe(order(['vendor.js', 'templates.js', 'app.js']))
    .pipe(concat('widget.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
});

gulp.task('css', function() {
    var src = [
        'bower_components/fullcalendar/dist/fullcalendar.css',
        'bower_components/tingle/dist/tingle.css',
        'src/main.css'
    ];

    return gulp.src(src)
    .pipe(autoprefixer({
        browsers: ['> 1%', 'last 3 versions'],
        cascade: false
    }))
    .pipe(concat('widget.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('./dist'));
});
